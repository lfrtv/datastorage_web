require 'open-uri'
require 'json'

class ArticleController < ApplicationController

	@@uri = ENV['API_URI'].nil? ? "https://datastorageapp.herokuapp.com" : ENV['API_URI']

	def index
		url = "#{@@uri}/articles.json?title=#{params[:title]}"
		response = open(url).read
		@articles = JSON.parse(response)
	end
end